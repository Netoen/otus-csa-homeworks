locals {
  hw2_bucket_prefix = "hw2"
}

resource "aws_s3_bucket" "s3_hw2_free" {
  bucket        = join("-", [local.hw2_bucket_prefix, "free"])
  force_destroy = false
}

resource "aws_s3_bucket" "s3_hw2_paid" {
  bucket        = join("-", [local.hw2_bucket_prefix, "paid"])
  force_destroy = false
}

resource "aws_s3_bucket_policy" "allow_access_to_free_s3" {
  bucket = aws_s3_bucket.s3_hw2_free.id
  policy = data.aws_iam_policy_document.allow_access_to_free_s3.json
}

resource "aws_s3_bucket_policy" "allow_access_to_paid_s3" {
  bucket = aws_s3_bucket.s3_hw2_paid.id
  policy = data.aws_iam_policy_document.allow_access_to_paid_s3.json
}

data "aws_iam_users" "iam_users_free" {
  path_prefix = "/free"
}

data "aws_iam_users" "iam_users_paid" {
  path_prefix = "/paid"
}

data "aws_iam_policy_document" "allow_access_to_free_s3" {
  statement {
    principals {
      type        = "AWS"
      identifiers = concat(
        tolist(data.aws_iam_users.iam_users_free.arns),
        tolist(data.aws_iam_users.iam_users_paid.arns)
      )
    }

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
      "s3:DeleteObject"
    ]

    resources = [
      aws_s3_bucket.s3_hw2_free.arn,
      "${aws_s3_bucket.s3_hw2_free.arn}/*",
    ]
  }
}

data "aws_iam_policy_document" "allow_access_to_paid_s3" {
  statement {
    principals {
      type        = "AWS"
      identifiers = data.aws_iam_users.iam_users_paid.arns
    }

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
      "s3:DeleteObject"
    ]

    resources = [
      aws_s3_bucket.s3_hw2_paid.arn,
      "${aws_s3_bucket.s3_hw2_paid.arn}/*",
    ]
  }
}