resource "aws_vpc_peering_connection" "central-to-left" {
  vpc_id = aws_vpc.vpc_hw3["central"].id
  peer_vpc_id = aws_vpc.vpc_hw3["left"].id
  auto_accept = true
  tags = {"Name" = "central-to-left"}
}

resource "aws_vpc_peering_connection" "central-to-right" {
  vpc_id = aws_vpc.vpc_hw3["central"].id
  peer_vpc_id = aws_vpc.vpc_hw3["right"].id
  auto_accept = true
  tags = {"Name" = "central-to-right"}
}