resource "aws_vpc" "vpc_hw3" {
  for_each = var.vpcs

  tags = {"Name" = each.key }
  cidr_block                            = each.value.cidr_block
  assign_generated_ipv6_cidr_block      = false
  enable_dns_hostnames                  = true
  enable_dns_support                    = true
  enable_network_address_usage_metrics  = false
}
