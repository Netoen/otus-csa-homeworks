resource "aws_default_security_group" "sg_hw3" {
  for_each = aws_vpc.vpc_hw3

  tags = {"Name" = each.key}
  vpc_id = each.value.id
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_vpc_security_group_egress_rule" "sgrer_hw3" {
  for_each = aws_default_security_group.sg_hw3

  description = "OUT to ANY for all"
  tags = {"Name" = "OUT to ANY"}
  security_group_id = each.value.id
  ip_protocol = -1
  cidr_ipv4 = "0.0.0.0/0"
}

resource "aws_vpc_security_group_ingress_rule" "sgrir_hw3" {
  for_each = aws_default_security_group.sg_hw3

  description = "IN from SELF for all"
  tags = {"Name" = "IN from SELF"}
  security_group_id = each.value.id
  ip_protocol = -1
  referenced_security_group_id = each.value.id
}

resource "aws_vpc_security_group_ingress_rule" "sgrir_hw3_central_left" {
  description = "IN from the LEFT"
  tags = {"Name" = "IN from the LEFT"}
  security_group_id = aws_default_security_group.sg_hw3["central"].id
  ip_protocol = -1
  referenced_security_group_id = aws_default_security_group.sg_hw3["left"].id
}

resource "aws_vpc_security_group_ingress_rule" "sgrir_hw3_central_right" {
  description = "IN from the RIGHT"
  tags = {"Name" = "IN from the RIGHT"}
  security_group_id = aws_default_security_group.sg_hw3["central"].id
  ip_protocol = -1
  referenced_security_group_id = aws_default_security_group.sg_hw3["right"].id
}

resource "aws_vpc_security_group_ingress_rule" "sgrir_hw3_left_central" {
  description = "IN from the CENTRAL"
  tags = {"Name" = "IN from the CENTRAL"}
  security_group_id = aws_default_security_group.sg_hw3["left"].id
  ip_protocol = -1
  referenced_security_group_id = aws_default_security_group.sg_hw3["central"].id
}

resource "aws_vpc_security_group_ingress_rule" "sgrir_hw3_right_central" {
  description = "IN from the LEFT"
  tags = {"Name" = "IN from the LEFT"}
  security_group_id = aws_default_security_group.sg_hw3["right"].id
  ip_protocol = -1
  referenced_security_group_id = aws_default_security_group.sg_hw3["central"].id
}