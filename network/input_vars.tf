variable "vpcs" {
  type = map(object({
    cidr_block = string
  }))
}

variable "ec2_instances" {
  type = set(string)
}