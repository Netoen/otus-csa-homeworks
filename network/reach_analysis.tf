data "aws_instance" "ec2_instance" {
  for_each = var.ec2_instances

  filter {
    name   = "tag:Name"
    values = [ each.value ]
  }
}

data "aws_instances" "all_ec2_instances" {
  filter {
    name    = "tag:Name"
    values  = var.ec2_instances
  }
}

locals {
  analyzer_directions = flatten([
    for src in data.aws_instances.all_ec2_instances.ids : [
      for dest in setsubtract(toset(data.aws_instances.all_ec2_instances.ids), toset( [src] )) : {
        src = src
        dest = dest
        # desc = "${data.aws_instance.ec2_instance}"
    }
  ]])
}

resource "aws_ec2_network_insights_path" "path" {
  count = length(local.analyzer_directions)

  source      = local.analyzer_directions[count.index]["src"]
  destination = local.analyzer_directions[count.index]["dest"]
  protocol    = "tcp"
}

resource "aws_ec2_network_insights_analysis" "analysis" {
  count = length(aws_ec2_network_insights_path.path)
  network_insights_path_id = aws_ec2_network_insights_path.path[count.index].id
}