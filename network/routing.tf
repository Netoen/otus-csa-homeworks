resource "aws_subnet" "subnet_hw3" {
  for_each = var.vpcs

  vpc_id = aws_vpc.vpc_hw3[each.key].id
  cidr_block = aws_vpc.vpc_hw3[each.key].cidr_block
  tags = {"Name" = each.key}
}

resource "aws_default_route_table" "rtb_hw3_central" {
  # vpc_id = aws_vpc.vpc_hw3["central"].id
  default_route_table_id = aws_vpc.vpc_hw3["central"].default_route_table_id
  route {
    cidr_block = aws_vpc.vpc_hw3["central"].cidr_block
    gateway_id = "local"
  }
  route {
    cidr_block = aws_vpc.vpc_hw3["left"].cidr_block
    vpc_peering_connection_id = aws_vpc_peering_connection.central-to-left.id
  }
  route {
    cidr_block = aws_vpc.vpc_hw3["right"].cidr_block
    vpc_peering_connection_id = aws_vpc_peering_connection.central-to-right.id
  }
  tags = {"Name" = "central"}
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_default_route_table" "rtb_hw3_left" {
  # vpc_id = aws_vpc.vpc_hw3["left"].id
  default_route_table_id = aws_vpc.vpc_hw3["left"].default_route_table_id
  route {
    cidr_block = aws_vpc.vpc_hw3["left"].cidr_block
    gateway_id = "local"
  }
  route {
    cidr_block = aws_vpc.vpc_hw3["central"].cidr_block
    vpc_peering_connection_id = aws_vpc_peering_connection.central-to-left.id
  }
  tags = {"Name" = "left"}
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_default_route_table" "rtb_hw3_right" {
  # vpc_id = aws_vpc.vpc_hw3["right"].id
  default_route_table_id = aws_vpc.vpc_hw3["right"].default_route_table_id
  route {
    cidr_block = aws_vpc.vpc_hw3["right"].cidr_block
    gateway_id = "local"
  }
  route {
    cidr_block = aws_vpc.vpc_hw3["central"].cidr_block
    vpc_peering_connection_id = aws_vpc_peering_connection.central-to-right.id
  }
  tags = {"Name" = "right"}
  lifecycle {
    create_before_destroy = true
  }
}
