terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  # shared_config_files = ["C:/Users/Netoen/.aws/config"]
  shared_credentials_files  = ["C:/Users/Netoen/.aws/credentials"]
  profile                   = "default"
  region                    = "eu-north-1"
  default_tags {
    tags = {
      owner = "asmakhno",
      actor = "terraform",
      course = "CSA-2022-03",
      activity = "homework"
    }
  }
}

module "network" {
  source = "./network"
  vpcs = {
    central = {cidr_block: "172.40.0.0/16"}
    left    = {cidr_block: "172.50.0.0/16"}
    right   = {cidr_block: "172.60.0.0/16"}
  }
  ec2_instances = [
    "app_central",
    "app_left",
    "app_right"
  ]
}

# module "storage" {
#   source = "./storage"
# }

# module "users" {
#   source = "./users"
# }
